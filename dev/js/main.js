/*----- Slick Slider Initialization -----*/
$(document).ready(function(){
    $('.slick-slider').slick();
});

/*----- ScrollWatch Plugin Initialization -----*/

$(document).ready(function() {
    var sw = new ScrollWatch();
});

/*----- Header Dropdown Menu -----*/

$(document).ready(function() {
    $('#dropdown-menu li').on('click', function() {
        $(this).children('.nav-submenu').stop(true, false, true).fadeToggle(300);
    });
});
